import React from 'react';
import FavoriteButton from '../FavoriteButton/FavoriteButton';
import {Link} from 'react-router-dom';
import {IMAGE_PATH_URL} from '../const';

export const MovieCard = ({movie}) => {
  if (!movie) {
    return;
  }

  const {title, poster_path, id, vote_count} = movie;
  const moviePoster = IMAGE_PATH_URL + poster_path;

  const renderGenres = movie.genres.map(genre => {
    if (!genre) {
      return null;
    }
    return (
        <span className="badge badge-info mr-2" key={genre.id}>{genre.name}</span>
    );
  });

  return (
      <div className="card col-3">
        <FavoriteButton id={id}/>
        <p>Votes: {vote_count}</p>

        <Link to={`/movie/${id}`}>
          <img className="card-img-top"
               alt={title}
               src={moviePoster}/>
          <div className="card-body">
            <h5 className="card-title">
              {title}
            </h5>

            <p>{renderGenres}</p>
          </div>
        </Link>
      </div>
  );
};
