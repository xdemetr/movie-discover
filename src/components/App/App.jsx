import React from 'react';
import {Header} from '../Header/Header';
import MovieListPage from '../Pages/MovieListPage/MovieListPage';
import {Route} from 'react-router-dom';
import MoviePage from '../Pages/MoviePage/MoviePage';

export const App = () => {
  return (
      <div className="app">
        <Header/>

        <div className="container">
          <Route path='/' exact render={() => <MovieListPage/>}/>
          <Route path='/movie/:id' exact render={() => <MoviePage/>}/>
        </div>
      </div>
  );
};
