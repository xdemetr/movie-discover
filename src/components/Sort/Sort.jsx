import React from 'react';
import {getSortSelector} from '../../store/selectors';
import {getMovieList} from '../../store/movieActions';
import {compose} from 'redux';
import {connect} from 'react-redux';
import cn from 'classnames';

const Sort = ({getMovieList, currentSort}) => {
  const sortBy = (sort) => {
    getMovieList(1, sort);
  };

  const sortOptions = [
    {
      title: 'Popularity',
      key: 'popularity',
    },
    {
      title: 'Vote',
      key: 'vote_count',
    },
    {
      title: 'Release',
      key: 'release_date',
    },
  ];

  const renderSort = sortOptions.map(item => {
    const sortOption = currentSort.split('.')[0];
    const sortDirection = currentSort.split('.')[1];

    const setSortDirection = () => {
      return sortDirection === 'desc' ? 'asc' : 'desc';
    };

    const SortIcon = () => {
      if (item.key !== sortOption) {
        return null;
      }

      return <span className="ml-3">{sortDirection === 'desc' ? 'desc' : 'asc'}</span>;
    };

    return (
        <li key={item.key}
            onClick={() => sortBy(`${item.key}.${setSortDirection()}`)}
            className={cn('list-group-item', 'list-group-item-action', item.key === sortOption && 'active')}>
          {item.title}
          <SortIcon/>
        </li>
    );
  });

  return (
      <div className="sort">
        <h3>Sort by</h3>
        <ul className="list-group">
          {renderSort}
        </ul>
      </div>
  );
};

const mapStateToProps = (state) => ({
  currentSort: getSortSelector(state),
});

const mapDispatchToProps = {
  getMovieList,
};

export default compose(connect(mapStateToProps, mapDispatchToProps))(Sort);
