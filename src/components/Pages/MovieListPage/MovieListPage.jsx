import React, {useEffect} from 'react';
import {compose} from 'redux';
import {connect} from 'react-redux';
import {getGenres, getMovieList} from '../../../store/movieActions';
import {getMoviesLoadingSelector, getMoviesReselect} from '../../../store/selectors';
import Paginator from '../../Paginator/Paginator';
import Sort from '../../Sort/Sort';
import {MovieList} from '../../MovieList/MovieList';

const MovieListPage = ({getMovieList, getGenres, movies, loading}) => {
  useEffect(() => {
    getGenres();
    getMovieList();
  }, [getMovieList, getGenres]);

  if (!movies) {
    return null;
  }

  return (
      <div className="row mb-3">
        <div className="col-3">
          <Sort/>
        </div>

        <div className="col-9">
          <Paginator/>
          <MovieList movies={movies} loading={loading}/>
        </div>
      </div>
  );
};

const mapStateToProps = (state) => ({
  movies: getMoviesReselect(state),
  loading: getMoviesLoadingSelector(state),
});

const mapDispatchToProps = {
  getMovieList,
  getGenres,
};

export default compose(connect(mapStateToProps, mapDispatchToProps))(MovieListPage);
