import React, {useEffect} from 'react';
import {Link, withRouter} from 'react-router-dom';
import {compose} from 'redux';
import FavoriteButton from '../../FavoriteButton/FavoriteButton';
import {getGenresSelector, getMovieSelector, getMoviesLoadingSelector} from '../../../store/selectors';
import {getGenres, getMovie, getMovieList} from '../../../store/movieActions';
import {connect} from 'react-redux';
import {IMAGE_PATH_URL} from '../../const';

const MoviePage = (props) => {
  const {id} = props.match.params;
  const {getMovie, movie} = props;


  useEffect(() => {
    getMovie(id);
  }, [id, getMovie]);

  if (!movie) {
    return null;
  }

  const {title, overview, poster_path, release_date} = movie;
  const moviePoster = IMAGE_PATH_URL + poster_path;

  return (
      <div className="movie-page">

        <div className="row">
          <div className="col-4">
            <img src={moviePoster} alt={title}/>
          </div>
          <div className="col-8">
            <Link to={'/'} className="btn-link mb-3 d-inline-block">&larr; Home</Link>
            <h1>{title}</h1>
            <h3>Release: {release_date}</h3>
            <p>{overview}</p>

            <FavoriteButton id={id}/>

          </div>
        </div>
      </div>
  );
};

const mapStateToProps = (state) => ({
  movie: getMovieSelector(state),
  genres: getGenresSelector(state),
  loading: getMoviesLoadingSelector(state),
});

const mapDispatchToProps = {
  getMovieList,
  getGenres,
  getMovie,
};

export default compose(connect(mapStateToProps, mapDispatchToProps), withRouter)(MoviePage);
