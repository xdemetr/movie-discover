import React from 'react';
import {getMoviesCurrentPage, getMoviesTotalPages} from '../../store/selectors';
import {getGenres, getMovieList} from '../../store/movieActions';
import {compose} from 'redux';
import {connect} from 'react-redux';
import cn from 'classnames';
import './pagination.css';

const Paginator = (props) => {
  const {current, total, getMovieList} = props;
  let pages = [];

  for (let i = 1; i <= total; i++) {
    pages.push(i);
  }

  return (
      <ul className="pagination">
        {
          pages.map((p) => {
            return (
                <li className={cn('page-item', current === p && 'active')} key={p}>
                  <span
                      className="page-link"
                      onClick={(e) => getMovieList(p)}>{p}</span>
                </li>
            );
          })
        }
      </ul>

  );
};

const mapStateToProps = (state) => ({
  current: getMoviesCurrentPage(state),
  total: getMoviesTotalPages(state),
});

const mapDispatchToProps = {
  getMovieList,
  getGenres,
};

export default compose(connect(mapStateToProps, mapDispatchToProps))(Paginator);
