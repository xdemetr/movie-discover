import React from 'react';
import {getFavoritesSelector} from '../../store/selectors';
import {toggleFavoriteMovie} from '../../store/movieActions';
import {compose} from 'redux';
import {connect} from 'react-redux';
import cn from 'classnames';

export const FavoriteButton = ({id, favorites, toggleFavoriteMovie}) => {
  const isFavorite = favorites.filter(i => i === +id).length;
  const buttonText = isFavorite ? 'Remove' : 'Add to favorite';

  return (
      <div className={cn({
        'btn': true,
        'mt-2 mb-2': true,
        'btn-outline-danger': isFavorite,
        'btn-outline-primary': !isFavorite,
      })}
           onClick={() => toggleFavoriteMovie(+id)}>
        {buttonText}
      </div>
  );
};

const mapStateToProps = (state) => ({
  favorites: getFavoritesSelector(state),
});

const mapDispatchToProps = {
  toggleFavoriteMovie,
};

export default compose(connect(mapStateToProps, mapDispatchToProps))(FavoriteButton);
