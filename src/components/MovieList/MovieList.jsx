import React from 'react';
import {MovieCard} from '../MovieCard/MovieCard';

export const MovieList = ({movies, loading}) => {
  if (loading) {
    return <span>loading</span>;
  }

  const renderMovies = movies.map(m => {
    return (
        <MovieCard movie={m} key={m.id}/>
    );
  });

  return (
      <div className="movie-list row">
        {renderMovies}
      </div>
  );
};
