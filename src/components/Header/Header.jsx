import React from 'react';

export const Header = () => {
  return (
      <header className="navbar-dark bg-primary mb-3">
        <div className="container">
          <span className="navbar-brand">Movie Discover</span>
        </div>
      </header>
  );
};
