import axios from 'axios';
import {API_KEY, API_URL} from '../components/const';

const instance = axios.create({
  baseURL: API_URL,
  params: {'api_key': API_KEY},
});

export const movieAPI = {
  getGenres() {
    return instance.get('genre/movie/list')
        .then(res => {
          return res.data.genres;
        });
  },

  getMovies(pageNum = 1, sortBy) {
    return instance.get('discover/movie', {
      params: {page: pageNum, sort_by: sortBy},
    })
        .then(res => res.data);
  },

  getMovie(id) {
    return instance.get(`/movie/${id}`)
        .then(res => res.data);
  },
};
