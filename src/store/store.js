import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import thunk from 'redux-thunk';
import movieReducer from './movieReducer';

const reducers = combineReducers({
  movies: movieReducer,
});

const composeEnhancers = ((window).__REDUX_DEVTOOLS_EXTENSION__ && (window).__REDUX_DEVTOOLS_EXTENSION__()) || compose;

const store = createStore(
    reducers,
    compose(
        applyMiddleware(thunk),
        composeEnhancers,
    ),
);

export default store;
