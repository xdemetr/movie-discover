import {createSelector} from 'reselect';

const getMovies = (state) => {
  return state.movies;
};

export const getGenresSelector = (state) => {
  return state.movies.genres;
};

export const getMoviesReselect = createSelector(getMovies, (movies) => {
  return movies.movies.results;
});

export const getMoviesLoadingSelector = (state) => {
  return state.movies.loading;
};

export const getMoviesCurrentPage = (state) => {
  return state.movies.movies.page;
};

export const getMoviesTotalPages = (state) => {
  return state.movies.movies.total_pages;
};

export const getMovieSelector = (state) => {
  return state.movies.movie;
};

export const getSortSelector = (state) => {
  return state.movies.sortBy;
};

export const getFavoritesSelector = (state) => {
  return state.movies.favorites;
};
