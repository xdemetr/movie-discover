import {
  FETCH_GENRES_SUCCESS,
  FETCH_MOVIE_SUCCESS,
  FETCH_MOVIES_REQUEST,
  FETCH_MOVIES_SUCCESS,
  SET_MOVIE_FAVORITE, SET_SORTING,
} from './movieReducer';
import {movieAPI} from '../api/api';

const movieListRequested = () => ({
  type: FETCH_MOVIES_REQUEST,
});

const movieListLoaded = (movies) => ({
  type: FETCH_MOVIES_SUCCESS,
  payload: movies,
});

const genresLoaded = (genres) => ({
  type: FETCH_GENRES_SUCCESS,
  payload: genres,
});

const movieLoaded = (movie) => ({
  type: FETCH_MOVIE_SUCCESS,
  payload: movie,
});

const setSorting = (val) => ({
  type: SET_SORTING,
  payload: val,
});

export const toggleFavoriteMovie = (id) => ({
  type: SET_MOVIE_FAVORITE,
  payload: id,
});

export const getGenres = () => {
  return (dispatch) => {
    movieAPI.getGenres().then(res => dispatch(genresLoaded(res)));
  };
};

export const getMovieList = (pageNumber, sortBy) => {
  return (dispatch) => {
    dispatch(movieListRequested());

    if (sortBy) {
      dispatch(setSorting(sortBy));
    }

    movieAPI.getMovies(pageNumber, sortBy).then(res => dispatch(movieListLoaded(res)));
  };
};

export const getMovie = (id) => {
  return (dispatch) => {
    movieAPI.getMovie(id).then(res => dispatch(movieLoaded(res)));
  };
};
