const initialState = {
  error: '',
  loading: false,
  movies: [],
  genres: [],
  favorites: [],
  movie: null,
  sortBy: 'popularity.desc',
};

export const FETCH_MOVIES_REQUEST = 'FETCH_MOVIES_REQUEST';
export const FETCH_MOVIES_SUCCESS = 'FETCH_MOVIES_SUCCESS';
export const FETCH_GENRES_SUCCESS = 'FETCH_GENRES_SUCCESS';
export const SET_MOVIE_FAVORITE = 'SET_MOVIE_FAVORITE';
export const FETCH_MOVIE_SUCCESS = 'FETCH_MOVIE_SUCCESS';
export const SET_SORTING = 'SET_SORTING';

const transformGenres = (movies, state) => {
  const genres = state.genres;
  const results = movies.results.map(movie => {
    const modGenres = movie.genre_ids.map(genre => {
      return genres.find(i => i.id === genre);
    });
    movie = {...movie, genres: modGenres};
    return movie;
  });
  return {...movies, results};
};

const toggleFavorite = (movieId, state) => {
  let newFav = [];
  const existFavorite = state.find(pl => pl === movieId);

  if (existFavorite) {
    const idx = state.findIndex((pl) => pl === movieId);
    const newArr = [...state.slice(0, idx), ...state.slice(idx + 1)];
    newFav = newArr;
  } else {
    const newArr = [...state, +movieId];
    newFav = newArr;
  }
  return newFav;
};

const movieReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MOVIES_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case FETCH_MOVIES_SUCCESS:
      return {
        ...state,
        loading: false,
        movies: transformGenres(action.payload, state),
      };

    case FETCH_GENRES_SUCCESS:
      return {
        ...state,
        genres: action.payload,
      };

    case SET_MOVIE_FAVORITE:
      return {
        ...state,
        favorites: toggleFavorite(action.payload, state.favorites),
      };

    case FETCH_MOVIE_SUCCESS:
      return {
        ...state,
        movie: action.payload,
      };

    case SET_SORTING:
      return {
        ...state,
        sortBy: action.payload,
      };

    default:
      return state;
  }
};

export default movieReducer;
